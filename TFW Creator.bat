@ECHO OFF
setlocal enabledelayedexpansion
ECHO TFW Creator for TIF files in folder
ECHO Author: Szczepkowski Marek
ECHO Date: 25-06-2019
ECHO Version: 1.0
ECHO.

SET QGIS_ROOT=C:\Program Files\QGIS 2.14
REM Setup gdal_transalte.exe
SET PATH=%PATH%;%QGIS_ROOT%\bin
SET GDAL_DATA=%QGIS_ROOT%\share\gdal
SET COORDINATE=2180

REM Path to working dir
SET WORK=%cd%
if not exist "%WORK%\TFW" mkdir "%WORK%\TFW"

REM COUNTER FILES
dir /b *.tif 2> nul | find "" /v /c > tmp && set /p count=<tmp && del tmp && echo %count%
set /A Counter=1

FOR /F %%i IN ('dir /b "%WORK%\*.tif"') DO (
    ECHO.
	ECHO Processing  %%i    !Counter! / %count% FILES
	gdal_translate -co "TFW=YES" -a_srs EPSG:%COORDINATE% %%i TFW\%%i
	set /A Counter+=1			
)
ECHO.
ECHO Done
PAUSE